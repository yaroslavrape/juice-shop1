import requests

headers = {
    'Authorization': 'Token 7addd800d84db0f94778bf1f42c45b041801cf9b'
}

url = 'http://localhost:8080/api/v2/import-scan/'

data = {
   'active': True,
   'verified': True,
    'scan_type': 'Gitleaks Scan',
    'minimum_severity': 'Low',
    'engagement': 3
}

files = {
    'file': open('gitleaks.json', 'rb')
}

response = requests.post(url, headers=headers, data=data, files=files)

if response.status_code == 201:
    print('Scan results imported successfully')
else:
    print(f'Failed to import scan results: {response.content}')